package work.darren.itext.common.api;

import java.io.InputStream;

/**
 * @author darren
 * @date 2019-05-25
 */
public interface PdfGenerator {

    default String fontpath() {
        return "/fonts/Alibaba-PuHuiTi-Regular.otf";
    }


    default String html() {
        StringBuilder out = new StringBuilder();
        try (InputStream input = this.getClass().getResourceAsStream("/tpl/sample.html")) {
            byte[] b = new byte[4096];
            int    n;
            while ((n = input.read(b)) != -1) {
                out.append(new String(b, 0, n));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        ;
        return out.toString();
    }

    void generatePdf(String html, String outputFile) throws Exception;
}
