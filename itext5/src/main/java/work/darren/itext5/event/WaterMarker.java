package work.darren.itext5.event;

import com.itextpdf.text.Document;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.BaseFont;
import com.itextpdf.text.pdf.ColumnText;
import com.itextpdf.text.pdf.GrayColor;
import com.itextpdf.text.pdf.PdfPageEventHelper;
import com.itextpdf.text.pdf.PdfWriter;
import lombok.AllArgsConstructor;

/**
 * Itext5 实现水印
 *
 * @author darren
 * @date 2019-05-25
 */
@AllArgsConstructor
public class WaterMarker extends PdfPageEventHelper {

    private BaseFont baseFont;

    @Override
    public void onEndPage(PdfWriter writer, Document document) {

        //水印文字
        Phrase waterMarkerText = new Phrase("WATER MARKER 文字水印", new Font(baseFont, 30, Font.NORMAL, new GrayColor(0.9F)));

        for (int i = 0; i < 5; i++) {
            for (int j = 0; j < 5; j++) {
                ColumnText.showTextAligned(writer.getDirectContentUnder(), Element.ALIGN_CENTER, waterMarkerText, (150 + i * 300), (160 + j * 150), 45);
            }
        }
    }
}
