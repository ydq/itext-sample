package work.darren.itext5.event;

import com.itextpdf.text.Document;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.BaseFont;
import com.itextpdf.text.pdf.ColumnText;
import com.itextpdf.text.pdf.GrayColor;
import com.itextpdf.text.pdf.PdfContentByte;
import com.itextpdf.text.pdf.PdfPageEventHelper;
import com.itextpdf.text.pdf.PdfWriter;
import lombok.AllArgsConstructor;

/**
 * Itext5 实现页码
 *
 * @author darren
 * @date 2019-05-25
 */
@AllArgsConstructor
public class PageMarker extends PdfPageEventHelper {

    private BaseFont baseFont;

    @Override
    public void onEndPage(PdfWriter writer, Document document) {

        PdfContentByte contentByte = writer.getDirectContent();
        contentByte.saveState();
        contentByte.beginText();
        contentByte.setFontAndSize(baseFont, 12);
        contentByte.showTextAligned(PdfContentByte.ALIGN_CENTER, "第" + document.getPageNumber() + "页", document.right(-40) / 2, document.bottom(-20), 0);
        contentByte.endText();
        contentByte.restoreState();
    }
}
