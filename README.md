# Itext-Sample

ItextPDF的简单示例



项目结构说明：

- common 模块：包含示例中公共的中文字体、HTML模板，提供公共的接口定义和工具类
- itext5模块：itext5的HTML转PDF实现示例，包含水印和页码
- itext7模块：itext7的HTML转PDF实现示例，包含水印和页码
- run模块：简单的测试用例模块



温馨提示：项目使用了lombok，所以IDE需要安装lombok插件(Java开发必备了吧)