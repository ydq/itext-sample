package work.darren.itext7.generator;

import com.itextpdf.html2pdf.ConverterProperties;
import com.itextpdf.html2pdf.HtmlConverter;
import com.itextpdf.html2pdf.attach.impl.OutlineHandler;
import com.itextpdf.kernel.events.PdfDocumentEvent;
import com.itextpdf.kernel.font.PdfFont;
import com.itextpdf.kernel.geom.PageSize;
import com.itextpdf.kernel.pdf.PdfDocument;
import com.itextpdf.kernel.pdf.PdfWriter;
import com.itextpdf.kernel.pdf.WriterProperties;
import com.itextpdf.layout.font.FontProvider;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import work.darren.itext.common.api.PdfGenerator;
import work.darren.itext7.event.PageMarker;
import work.darren.itext7.event.WaterMarker;

/**
 * Itext7 实现HTML转PDF
 *
 * @author darren
 * @date 2019-05-25
 */
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class Itext7Generator implements PdfGenerator {

    private FontProvider fontProvider;

    private volatile static Itext7Generator instanse;

    public static PdfGenerator getInstanse() {
        if (instanse == null) {
            synchronized (Itext7Generator.class) {
                if (instanse == null) {
                    instanse = new Itext7Generator();
                    instanse.initFont();
                }
            }
        }

        return instanse;
    }

    private void initFont() {
        fontProvider = new FontProvider();
        //设置中文字体文件的路径，可以在classpath目录下
        fontProvider.addFont(fontpath());
    }

    @Override
    public void generatePdf(String html, String outputFile) throws Exception {
        //outputFile也可以是输出流
        PdfWriter   writer = new PdfWriter(outputFile, new WriterProperties().setFullCompressionMode(Boolean.TRUE));
        PdfDocument doc    = new PdfDocument(writer);
        doc.setDefaultPageSize(PageSize.A4);
        doc.getDefaultPageSize().applyMargins(20, 20, 20, 20, true);

        //获取字体，提供给水印 和 页码使用
        PdfFont pdfFont = fontProvider.getFontSet()
                .getFonts()
                .stream()
                .findFirst()
                .map(fontProvider::getPdfFont)
                .orElse(null);

        doc.addEventHandler(PdfDocumentEvent.END_PAGE, new WaterMarker(pdfFont));

        doc.addEventHandler(PdfDocumentEvent.END_PAGE, new PageMarker(pdfFont));

        ConverterProperties properties = new ConverterProperties();
        properties.setFontProvider(fontProvider);
        //PDF目录
        properties.setOutlineHandler(OutlineHandler.createStandardHandler());
        HtmlConverter.convertToPdf(html, doc, properties);
    }
}
